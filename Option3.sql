/*1*/
select mName from  tBookMakers where mName like '��%'
/*2*/
select mName , mAddress from  tBookMakers where mAddress is not null
/*3*/
select m.mName , avg(b.bPrice) from  tBookMakers m, tBooks b where b.bBookMaker=m.mID group by m.mName
/*4*/
select m.mName , sum(b.bOrdered) from  tBookMakers m, tBooks b where b.bBookMaker=m.mID group by m.mName
/*5*/
select m.mName ,m.mAddress from  tBookMakers m, tBooks b where b.bBookMaker=m.mID and b.bOrdered>4
/*6*/
select m.mName ,m.mID from  tBookMakers m, tBooks b where b.bBookMaker=m.mID and b.bOrdered=(select max(bOrdered) from tBooks)
/*7*/
select m.mName, sum(b.bOrdered)  from  tBookMakers m, tBooks b where b.bBookMaker=m.mID group by m.mName