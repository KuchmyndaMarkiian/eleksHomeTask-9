create table tAuthors
(
 aID int primary key identity(1,1) not null,
 aName varchar(50),
 aAddress varchar(50)
)

create table tGenres
(
 gID int primary key identity(1,1) not null,
 gName varchar(50)
)

create table tBookMakers
(
 mID int primary key identity(1,1) not null,
 mName varchar(50),
 mAddress varchar(50)
)

create table tBooks
(
 bID int primary key identity(1,1) not null,
 bName varchar(50),
 bAuthor int,
  bGenre int,
   bBookMaker int,
    bPrice real,
	 bAmount int,
	  bOrdered int
)
